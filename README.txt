Run Instructions

Working directory: cdr/

Clustering:
    python clustering.py --data (fb|bank) --method (kmeans|em) [--reducer (pca, ica, rps, lle)]

Dimensionality Reduciton:
    python reducing.py --data (fb|bank) --reducer (pca, ica, rps, lle)

Neural Network Training:
    python neural_network.py --data (fb|bank) [--method (kmeans|em)] [--reducer (pca, ica, rps, lle)]
