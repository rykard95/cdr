from data import get_bank_campaign_data, get_facebook_data
from sklearn.decomposition import PCA
from sklearn.random_projection import GaussianRandomProjection
from sklearn.decomposition import FastICA
from sklearn.manifold import LocallyLinearEmbedding
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot as plt
import time
import seaborn as sbn

sbn.set_style("darkgrid")

def reduce(method, x, n_components=2):
    reducer = None
    if method == "pca":
        reducer = PCA(n_components=n_components)
    elif method == "rps":
        reducer = GaussianRandomProjection(n_components=n_components)
    elif method == "ica":
        reducer = FastICA(n_components=n_components, whiten=True, max_iter=1000)
    elif method == "lle":
        reducer = LocallyLinearEmbedding(n_components=n_components)
    reducer.fit(x)
    return reducer


def score(method, reducer, x):
    if method in ("pca", "ica"):
        x_hat = reducer.inverse_transform(reducer.transform(x))
    elif method == "rps":
        x_hat = invert_random_projection(reducer.transform(x), reducer.components_)
    elif method == "lle":
        return reducer.reconstruction_error_

    return mean_squared_error(x, x_hat)


def invert_random_projection(h, W):
    return np.dot(h, np.linalg.inv(W.dot(W.T)).dot(W))


def truncate_pca(pca_reducer):
    power = np.cumsum(pca_reducer.explained_variance_ratio_)
    num_components_to_keep = np.where(power >= .8)[0][0] + 1
    return num_components_to_keep


def generate_iteration_plot(method, x, data_name, title, show=False):
    title_map = {"rps": "Random Projections",
                 "pca": "Principle Component Analysis",
                 "lle": "Local Linear Embedding",
                 "ica": "Independent Component Analysis"}
    reconstruction_errors = []
    fitting_times = []
    num_components = np.arange(1, x.shape[1] + 1)
    if method == "lle":
        num_components = np.arange(1, 5)
    elif method == "ica":
        num_components = np.arange(1, min(15, x.shape[1]+1))

    for i in num_components:
        tic = time.time()
        reducer = reduce(method, x, n_components=i)
        toc = time.time()
        fitting_time = toc - tic
        fitting_times.append(fitting_time)
        reconstruction_error = score(method, reducer, x)
        reconstruction_errors.append(reconstruction_error)
        print("applying: {} - n_components: {} - reconstrution_error: {} - fitting_time: {}".format(method, i,
                                                                                                    reconstruction_error,
                                                                                                    fitting_time))

    df = pd.DataFrame({"num_components": num_components, "reconstruction_error": reconstruction_errors,
                       "fitting_times": fitting_times})
    ax = sbn.lineplot(x="num_components", y="reconstruction_error", data=df)
    ax.set_title(title.format(title_map[method]))
    ax.set(xlabel="Iteration Number", ylabel="Reconstruction MSE")
    plt.savefig("plots/{}_{}.png".format(method, data_name))
    if show:
        plt.show()
    plt.clf()

    ax = sbn.lineplot(x="num_components", y="fitting_times", data=df)
    ax.set_title(title.format(title_map[method]))
    ax.set(xlabel="Iteration Number", ylabel="Fitting Time (s)")
    plt.savefig("plots/{}_{}_time.png".format(method, data_name))

    if show:
        plt.show()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--data", required=True, type=str, help="The dataset to cluster.")
    parser.add_argument("--reducer", required=True, type=str, help="If performing a reduction, specify it here.")

    args = parser.parse_args()

    if args.data == "fb":
        data, data_name, title = get_facebook_data(), "fb", "Facebook Data: {}"

    elif args.data == "bank":
        data, data_name, title = get_bank_campaign_data(), "bank", "Bank Campaign Data: {}"

    x = data.drop("label", axis=1)
    generate_iteration_plot("rps", x, data_name, title)
