from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from sklearn.metrics import silhouette_score
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import time
from reducing import reduce

from data import get_facebook_data, get_bank_campaign_data
import seaborn as sbn

sbn.set_style("darkgrid")


def cluster(method, x, args={"n_components": 1}):
    if method == "kmeans":
        clustering = KMeans(n_clusters=args["n_clusters"])
    elif method == "em":
        clustering = GaussianMixture(n_components=args["n_components"])
    clustering.fit(x)

    return clustering


def auto_k_means(x):
    scores = []
    times = []
    start = 2
    end = 3
    for k in range(start, end):
        tic = time.time()
        clustering = cluster("kmeans", x, {"n_clusters": k})
        toc = time.time()
        clustering_times = toc - tic
        times.append(clustering_times)
        score = silhouette_score(x, clustering.labels_, sample_size=x.shape[0] // 2)

        print("applying: {} - k: {} - silhouette_score: {} - clustering_time: {}".format("kmeans", k, score,
                                                                                         clustering_times))
        scores.append(score)

    silhouettes = pd.DataFrame({"k": range(start, end), "score": scores, "times": times})

    best_k = np.argmax(scores) + start

    return cluster("kmeans", x, {"n_clusters": best_k}), best_k, silhouettes


def auto_em(x):
    scores = []
    times = []
    start = 2
    end = 3
    for k in range(start, end):
        tic = time.time()
        clustering = cluster("em", x, {"n_components": k})
        toc = time.time()
        clustering_times = toc - tic
        times.append(clustering_times)
        score = silhouette_score(x, clustering.predict(x), sample_size=x.shape[0] // 2)

        print("applying: {} - k: {} - silhouette_score: {} - clustering_time: {}".format("em", k, score,
                                                                                         clustering_times))
        scores.append(score)

    silhouettes = pd.DataFrame({"k": range(start, end), "score": scores, "times": times})

    best_k = np.argmax(scores) + start

    return cluster("em", x, {"n_components": best_k}), best_k, silhouettes


def cluster_plot(title, filename, df, show=False):
    plt.clf()
    ax = sbn.lineplot(x="k", y="score", data=df)
    ax.set(xlabel="K", ylabel="Silhouette Score", title=title)
    plt.savefig("plots/{}.png".format(filename))
    if show:
        plt.show()

    plt.clf()

    ax = sbn.lineplot(x="k", y="times", data=df)
    ax.set(xlabel="K", ylabel="Clustering Time (s)", title=title)
    plt.savefig("plots/{}-time.png".format(filename))

    if show:
        plt.show()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--data", required=True, type=str, help="The dataset to cluster.")
    parser.add_argument("--reducer", type=str, help="If performing a reduction, specify it here.")
    parser.add_argument("--method", required=True, type=str, help="The type of clustering to perform.")

    args = parser.parse_args()

    if args.data == "fb":
        dataset_name = "fb"
        data = get_facebook_data()
    x = data.drop("label", axis=1)
    if args.reducer:
        reducer = reduce(method=args.reducer, x=x)
        x = reducer.transform(x)

    if args.method == "kmeans":
        clustering, best_k, silhouettes = auto_k_means(x)
        silhouettes.to_csv("metrics/{}-kmeans-{}.csv".format(args.method, dataset_name))

    elif args.method == "em":
        clustering, best_k, silhouettes = auto_em(x)
        silhouettes.to_csv("metrics/{}-em-{}.csv".format(args.method, dataset_name))

