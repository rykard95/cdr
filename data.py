import pandas as pd

FACEBOOK_DATA_PATH = "data/facebook.csv"


def get_bank_campaign_data():
    bank_df = pd.read_csv("data/bank.csv")
    labels = [-1 if el == "no" else 1 for el in bank_df["success"]]
    bank_df = bank_df.drop("success", axis=1)

    # One-hot encode all non-numeric columns
    for column in bank_df.select_dtypes(include=[object]).columns:
        bank_df = pd.concat((bank_df, pd.get_dummies(bank_df[column], prefix=column)), axis=1)
        bank_df = bank_df.drop(column, axis=1)


    bank_df["label"] = labels
    bank_df = bank_df.dropna()
    print(bank_df.shape)
    return bank_df.sample(frac=1.0)


def get_facebook_data():
    facebook_df = pd.read_csv("data/facebook.csv", encoding="utf_8")
    labels = [-1 if el == "male" else 1 for el in facebook_df["gender"]]
    facebook_df = facebook_df.drop("gender", axis=1)
    facebook_df = facebook_df.drop("userid", axis=1)

    facebook_df["label"] = labels

    facebook_df = facebook_df.dropna()
    print(facebook_df.sample(frac=0.5).shape)
    return facebook_df.sample(frac=0.5)


if __name__ == "__main__":

    bank_df = get_bank_campaign_data()
    facebook_df = get_facebook_data()
    print(bank_df.describe().to_csv("data/bank_summary.csv"))
