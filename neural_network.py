from keras.models import Sequential
from keras.layers import Dense, Activation

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

from clustering import auto_k_means, cluster, auto_em
from reducing import reduce

from data import get_facebook_data, get_bank_campaign_data
import time
import pandas as pd
import numpy as np


def get_nn(x):
    model = Sequential([
        Dense(100, input_shape=(x.shape[1],)),
        Activation('softmax'),
        Dense(1),
        Activation('softmax')
    ])

    model.compile(optimizer='rmsprop',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model


def evaluate(dr_method, data, data_name, tag=""):
    x = data.drop("label", axis=1)
    y = data["label"]
    x_train, x_test, y_train, y_test = train_test_split(x, y)

    training_scores = []
    test_scores = []
    training_times = []
    n_components = [0]
    if dr_method is None:
        model = get_nn(x_train)
        tic = time.time()
        model.fit(x_train, y_train)
        toc = time.time()
        training_times = [toc - tic]
        training_scores = [accuracy_score(y_train, model.predict(x_train))]
        test_scores = [accuracy_score(y_test, model.predict(x_test))]
    else:
        n_components = np.arange(2, 11)
        if dr_method == "lle":
            n_components = np.arange(2, 5)
        for k in n_components:
            reducer = reduce(dr_method, x_train, n_components=k)
            x_train_ = reducer.transform(x_train)
            x_test_ = reducer.transform(x_test)
            model = get_nn(x_train_)

            tic = time.time()
            model.fit(x_train_, y_train)
            toc = time.time()

            training_time = toc - tic
            training_score = accuracy_score(y_train, model.predict(x_train_))
            test_score = accuracy_score(y_test, model.predict(x_test_))
            print("iteration: {} - training_time: {} - training_score: {} - test_score: {}".format(k, training_time,
                                                                                                   training_score,
                                                                                                   test_score))
            training_times.append(training_time)
            test_scores.append(test_score)
            training_scores.append(training_score)

    df = pd.DataFrame({"training_score": training_scores, "test_score": test_scores, "training_time": training_times,
                       "k": n_components})
    df.to_csv("metrics/{}-{}{}.csv".format(dr_method, data_name, tag))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--data", required=True, type=str, help="The dataset to cluster.")
    parser.add_argument("--reducer", type=str, help="If performing a reduction, specify it here.")
    parser.add_argument("--method", type=str, help="The type of clustering to perform.")

    args = parser.parse_args()
    method = None

    if args.data == "fb":
        data = get_facebook_data()

    elif args.data == "bank":
        data = get_bank_campaign_data()

    x = data.drop("label", axis=1)

    if args.method:
        if args.method == "kmeans":
            clustering, best_k, silhouette = auto_k_means(x)
        elif args.method == "em":
            clustering, best_k, silhouette = auto_em(x)

        x = clustering.predict_proba(x)
        x = pd.DataFrame(x)

    if args.reducer:
        reducer = reduce(method=args.reducer, x=x)
        x = reducer.transform(x)

    x["label"] = data["label"]

    evaluate(method, x, "fb", "-crem")
